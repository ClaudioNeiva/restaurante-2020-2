package br.ucsal.bes20202.testequalidade.restaurante.business;

public class RestauranteBOTest {

	/**
	 * Método a ser testado:
	 * 
	 * public void abrirComanda(Integer numeroMesa) throws RegistroNaoEncontrado,
	 * MesaOcupadaException.
	 * 
	 * Verificar se a abertura de uma comanda para uma mesa livre apresenta sucesso.
	 * 
	 * Atenção:
	 * 
	 * 1. Crie um builder para instanciar a classe Mesa;
	 * 
	 * 2. NÃO é necesário fazer o mock para o getSituacao;
	 * 
	 * 3. NÃO é necesário fazer o mock para o construtor de Comanda;
	 * 
	 * 4. Como o método abrirComanda é void, você deverá usar o verify para se
	 * certificar do sucesso o não da execução do teste. Não é necessário garantir
	 * que a comanda incluída é para a mesa informada, basta verificar se uma
	 * comanda foi incluída.
	 * 
	 */
	public void abrirComandaMesaLivre() {
	}
}
