package br.ucsal.bes20202.testequalidade.restaurante.business;

public class RestauranteBOIntegradoTest {

	/**
	 * Método a ser testado:
	 * 
	 * public void abrirComanda(Integer numeroMesa) throws RegistroNaoEncontrado,
	 * MesaOcupadaException.
	 * 
	 * Verificar se a abertura de uma comanda para uma mesa livre apresenta sucesso.
	 * 
	 * Atenção:
	 * 
	 * 1. Crie um builder para instanciar a classe Mesa;
	 * 
	 * 2. Como o método abrirComanda é void, você deverá utilizar o ComandaDAO para
	 * verificar se o método teve sucesso. É necessário verificar se a comanda foi
	 * incluída para a mesa informada.
	 * 
	 */
	public void abrirComandaMesaLivre() {
	}
}
